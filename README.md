# Pacman PKGBUILD for Eclipse IDE for Java and DSL Developers

## Installing

1. Run `makepkg -f`.
2. Run `pacman -U <package>`.

## Upgrading

1. Look for the newest version [here](https://www.eclipse.org/downloads/packages/).
2. Update version and download URL in PKGBUILD.
3. Update sha512sum in PKGBUILD.
4. Perform the steps listed in 'Installing'.
